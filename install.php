<?
$MODUL['wp_cms'] = ['URL' => 'https://gitlab.com/papp'];
$MODUL['wp_papp'] = ['URL' => 'https://gitlab.com/papp'];
$MODUL['wp_admin'] = ['URL' => 'https://gitlab.com/papp'];
$MODUL['wp_helpdesk'] = ['URL' => 'https://gitlab.com/papp'];
#==========================================
ini_set('error_reporting', E_ALL & ~E_NOTICE);
ini_set('memory_limit', '256M');
ini_set('max_execution_time', 120);
session_start();
$D = $_REQUEST['D']?:null;
#==========================================

class CFile
{
	static function write($url,$text,$modus=null)
	{
		$modus = ($modus == 'APPEND')?'FILE_APPEND':null;
		file_put_contents($url,$text, $modus);
	}

	static function copy($from, $to, $D = null)
	{
		$D['CHMOD'] = (!$D['CHMOD'])?0777:$D['CHMOD'];
		#Erstelle Verzeichnis fals nicht exsistiert
		$pi = pathinfo($to);
		CFile::mkdir($pi['dirname'], $D);
		
		if(is_array($from) && isset($from['tmp_name'])) #Ist Upload
		{
			for($i=0; $i < count($from['tmp_name']); $i++)
				move_uploaded_file($from['tmp_name'][$i], $to.'/'.$from['name'][$i]);
		}
		elseif(is_file($from)) #File
		{
			if(strpos($from,'http://') !== false )
				$from = str_replace(' ','%20',$from);
			copy($from,$to);
		}
		elseif(is_dir($from)) #Dir
		{
			$dh = opendir($from);
			while (($file = readdir($dh)) !== false)
			{
				if($file != '.' && $file != '..')
					if(is_file($from.$file))
						CFile::copy($from.$file,$to.$file,$D);
					else
						CFile::copy($from.$file.'/',$to.$file.'/',$D);
			}
		}
	}
	static function mkdir($pfad, $D=null)
	{
		$D['CHMOD'] = (!$D['CHMOD'])?0777:$D['CHMOD'];
		$D['RECURSIVE'] = (!$D['RECURSIVE'])?true:$D['RECURSIVE'];
		$oldumask = umask(0);
		@mkdir($pfad, $D['CHMOD'], $D['RECURSIVE']);
		umask($oldumask);
	}
	
	static function _dir($D)
	{
		if (is_dir($D['PATH']))
		{
			if ($dh = opendir($D['PATH']))
			{
				while (($file = readdir($dh)) !== false)
				{
					if($file != '.' && $file != '..')
						if(!is_file($D['PATH'] . $file)) #filetype($D['PATH'] . $file) == 'dir')
						{
							$FILE = CFile::_dir(['PATH' => $D['PATH'] . $file.'/']);
							$D['DIR'][ $file ] = array(
								'NAME'	=>	$file,
								'FILE'	=>	$FILE['FILE'],
								);
						}
						else
						{
							$pi = pathinfo($file);
							$fi = stat($D['PATH'].$file);
							#Nur bei jpeg, && Tiff
							if(strtolower($pi['extension']) == 'jpg' || strtolower($pi['extension']) == 'jpeg' || $pi['extension'] == 'tiff')
							{
								$rd = exif_read_data($D['PATH'].$file, 0, false);
								
								if($rd['DateTimeOriginal'])
									$recording_time = str_replace(array('-',' ',':'),array(''), $rd['DateTimeOriginal']);
								else if($rd['DateTimeDigitized'])
									$recording_time = str_replace(array('-',' ',':'),array(''), $rd['DateTimeDigitized']);
								else if($rd['DateTime'])
									$recording_time = str_replace(array('-',' ',':'),array(''), $rd['DateTime']);  
							}
							
							$D['FILE'][ $file ] = array(
								'NAME'			=> $file,
								'FILENAME'		=> $pi['filename'],
								'EXTENSION'		=> $pi['extension'],
								'SIZE'			=> $fi['size'], #filesize($D['PATH'].$file),
								'CREATE_TIME'	=> date('YmdHis',$fi['ctime']),
								'EDIT_TIME'		=> date('YmdHis',$fi['mtime']),
								'RECORDING_TIME'=> $recording_time,#aufnahme Datum
								);
						}
					
				}
				closedir($dh);
			}
		}
		return $D;
	}
	
	static function dir($dir)
	{
		if(is_dir($dir))
		{
			$d = dir($dir);
			while (false !== ($entry = $d->read()))
				if($entry != '..' && $entry != '.')
					$DIR[] = $entry;
			$d->close();
		}
		return $DIR;
	}
	
	static function unzip($file_zip, $to_dir)
	{
		$zip = new ZipArchive;
		$res = $zip->open($file_zip);
		if ($res === TRUE) {
		  $zip->extractTo($to_dir);
		  $zip->close();
		} else {
		  echo 'doh!';
		}
	}
	static function remove($from)
	{ 
		if(substr($from, -1, 1) == '/')
		$from .= '*';

		foreach (glob($from) as $filename)
		{
			if(is_file($filename) || is_link($filename))
				unlink($filename);
			else
			{
				CFile::remove("{$filename}/*");
				if(!rmdir($filename))
				{
					chmod($filename, 0777);
					rmdir($filename);
				}
			}
		}
	}
	
	static function symlink($from,$to)
	{
		$pi = pathinfo($to);
		CFile::mkdir($pi['dirname'], $D);
		
		if(is_file($from)) #File
		{
			$doc_root = CFile::getAbsoluteApplicationPath();#Windows fix
			symlink($doc_root.$from,$doc_root.$to);
		}
		elseif(is_dir($from)) #Dir
		{
			$dh = opendir($from);
			while (($file = readdir($dh)) !== false)
			{
				if($file != '.' && $file != '..')
					if(is_file($from.$file))
						CFile::symlink($from.$file,$to.$file,$D);
					else
						CFile::symlink($from.$file.'/',$to.$file.'/',$D);
			}
		}
	}
	
	static function getAbsoluteApplicationPath()
	{
		return dirname($_SERVER['SCRIPT_FILENAME']).'/';
	}
	static function getRelativeApplicationPath()
	{
		return dirname($_SERVER['PHP_SELF']);
	}
}

class CPApp
{
	function CPApp()
	{}
	
	function createPAppDir()
	{
		if(!is_dir('data'))
			mkdir('data',0777,true);
		if(!is_dir('system'))
			mkdir('system',0777,true);
		if(!is_dir('tmp'))
			mkdir('tmp',0777,true);
	}
	
	function createPAppFile()
	{
		#CFile::write('class.php',base64_decode('PD8NCmNsYXNzIGNsDQp7DQoJZnVuY3Rpb24gX19jb25zdHJ1Y3QoKQ0KCXsNCgkJZ2xvYmFsICREOw0KCQkkdGhpcy0+RCA9ICYkRDsNCgl9DQoJDQoJZnVuY3Rpb24gX19jYWxsKCRNLCAkZCkNCgl7DQoJCWlmKCBpbmNsdWRlX29uY2UoInRtcC9zeXN0ZW0vY2xhc3MveyRNfS5waHAiKSApDQoJCQlyZXR1cm4gJHRoaXMtPmNsWyRNXSA9ICR0aGlzLT5jbFskTV0gPzogbmV3ICRNKCR0aGlzLT5EKTsNCgkJZXhpdDsNCgl9DQp9'));
		#CFile::write('index.php',base64_decode('PD8NCmVycm9yX3JlcG9ydGluZyhFX0FMTCAmIH5FX05PVElDRSk7DQpzZXNzaW9uX3N0YXJ0KCk7DQoNCiREID0gJF9SRVFVRVNUWydEJ10/Om51bGw7DQokRFsnU0VTU0lPTiddID0gJiRfU0VTU0lPTlsnRCddOw0KDQppbmNsdWRlX29uY2UoImNsYXNzLnBocCIpOw0KJEMgPSBuZXcgY2woKTsNCg0KJERbJ1BBR0UnXSA9ICREWydQQUdFJ10/OiAnaW5kZXgnOw0KaW5jbHVkZV9vbmNlKCJ0bXAvc3lzdGVtL3skRFsnUEFHRSddfS5waHAiKTsNCiRwID0gbmV3ICREWydQQUdFJ10oKTsNCiRwLT5sb2FkKCk7DQokcC0+c2hvdygpOw=='));
		#CFile::write('.htaccess',base64_decode('I0RlZmxhdGUgQ29tcHJlc3Npb24NCjxJZk1vZHVsZSBtb2RfZGVmbGF0ZS5jPg0KCTxGaWxlc01hdGNoICJcXC4oanN8anBnfGpwZWd8Z2lmfHBuZ3xjc3N8aHRtfGh0bWx8eG1sKSQiPg0KCQlFeHBpcmVzQWN0aXZlIG9uDQoJCUV4cGlyZXNEZWZhdWx0ICJhY2Nlc3MgcGx1cyAxIG1vbnRoIg0KCQlTZXRPdXRwdXRGaWx0ZXIgREVGTEFURQ0KCTwvRmlsZXNNYXRjaD4NCjwvSWZNb2R1bGU+DQoNCiNTRU8gU1RBUlQNCjxJZk1vZHVsZSBtb2RfcmV3cml0ZS5jPg0KCVJld3JpdGVFbmdpbmUgT24NCglSZXdyaXRlQmFzZSAvdGVzdC9zY3JpcHQNCg0KCSM9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09DQoJI0xpYnJhcnkgW2xpYnJhcnkvLi4uXQkJCS0ganMsIGltZywgY3NzDQoJUmV3cml0ZVJ1bGUgXmxpYnJhcnkvKC4qKSQJCXRtcC9zeXN0ZW0vbGlicmFyeS8kMSBbUVNBXQ0KCSNkYXRhIFtkYXRhLy4uLl0JCQkJLSBVc2VyIERhdGEgKGltZyxwZGYsemlwLC4uLikNCglSZXdyaXRlUnVsZSBeZGF0YS8oLiopJAkJCWRhdGEvJDEgW1FTQV0NCgkjU0VPMiBbLi4uPy4uLl0gJiBbLi4uXQkJCS0gU0VPIHx8IGRpZXJla3QgWnVncmlmZiAoLnBocCkNCgkjUmV3cml0ZUNvbmQgJXtSRVFVRVNUX1VSSX0gIV4uK1wuKGluZGV4fHRtcHxkYXRhfGNzc3wuKnxqc3xnaWZ8anBnfHBuZ3xwZGYpJA0KCSNSZXdyaXRlQ29uZCAle1JFUVVFU1RfVVJJfSAhXi4rXC4oaW5kZXgpJA0KCVJld3JpdGVDb25kICV7UkVRVUVTVF9VUkl9ICFeKC4qKS8odG1wfGRhdGF8bGlicmFyeXxpbnN0YWxsLnBocCkNCgkjUmV3cml0ZVJ1bGUgXiguKik/KC4qKSQJCQlpbmRleC5waHA/RFtQQUdFXT1zZW9pbmRleCZEW1NFT19VUkxdPSQxJiQyIFtRU0FdDQoJUmV3cml0ZVJ1bGUgXiguKik/KC4qKSQJCQlpbmRleC5waHA/RFtTRU9fVVJMXT0kMSYkMiBbUVNBXQ0KCSM9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PQ0KPC9JZk1vZHVsZT4NCiNTRU8gRU5EDQo='));
		CFile::write('data/.htaccess','<Files "*.*">Deny from all</Files>');
		CFile::write('system/.htaccess','<Files "*.*">Deny from all</Files>');
		#CFile::write('tmp/.htaccess','<Files "*.*">Deny from all</Files>');
		CFile::write('class.php','<?php
class cl
{
	function __construct()
	{
		global $D;
		$this->D = &$D;
	}
	
	function __call($M, $d)
	{
		if( include_once("tmp/system/class/{$M}.php") )
			return $this->cl[$M] = $this->cl[$M] ?: new $M($this->D);
		exit;
	}
}');
		CFile::write('index.php','<?php
error_reporting(E_ALL & ~E_NOTICE);
session_start();
$D = $_REQUEST[\'D\']?:null;
$D[\'SESSION\'] = &$_SESSION[\'D\'];
include_once(\'!config.php\');
include_once(\'class.php\');
$C = new cl();
$D[\'PAGE\'] = $D[\'PAGE\']?:\'index\';
include_once("tmp/system/{$D[\'PAGE\']}.php");
$p = new $D[\'PAGE\']();
$p->load();
$p->show();');
		CFile::write('.htaccess','
#Deflate Compression
<IfModule mod_deflate.c>
	<FilesMatch "\\.(js|jpg|jpeg|gif|png|css|htm|html|xml)$">
		ExpiresActive on
		ExpiresDefault "access plus 1 month"
		SetOutputFilter DEFLATE
	</FilesMatch>
</IfModule>

#SEO START
<IfModule mod_rewrite.c>
	RewriteEngine On
	RewriteBase '.CFile::getRelativeApplicationPath().'

	#Library [library/...]			- js, img, css
	RewriteRule ^(.*)/library/(.*)$		tmp/system/library/$2 [QSA]
	RewriteRule ^library/(.*)$		tmp/system/library/$1 [QSA]
	#data [data/...]				- User Data (img,pdf,zip,...)
	RewriteRule ^(.*)/data/(.*)$			data/$2 [QSA]
	RewriteRule ^data/(.*)$			data/$1 [QSA]
	#SEO2 [...?...] & [...]			- SEO || dierekt Zugriff (.php)
	#RewriteCond %{REQUEST_URI} !^.+\.(index|tmp|data|css|.*|js|gif|jpg|png|pdf)$
	#RewriteCond %{REQUEST_URI} !^.+\.(index)$
	RewriteCond %{REQUEST_URI} !^(.*)/(tmp|data|library|install.php)
	#RewriteRule ^(.*)?(.*)$			index.php?D[PAGE]=seoindex&D[SEO_URL]=$1&$2 [QSA]
	RewriteRule ^(.*)?(.*)$			index.php?D[SEO_URL]=$1&$2 [QSA]
</IfModule>
#SEO END');
	}
	
	function getModulInfo($URL, $ID, $D=null)
	{
		if($URL == 'local')
			$arr = file_get_contents("system/{$ID}/modul.json");
		else
			$arr = file_get_contents("{$URL}/{$ID}/raw/master/modul.json");
		return json_decode($arr,1);
	}
	
	#Download Reqursive Modul mit abhängigen Modulen
	#ModID, D[URL] = SERVER_URL(EigenServer,GitLab)
	function rModulDownload($URL, $ID, $D=null)
	{
		$Mod = $this->getModulInfo($URL,$ID);
		$this->ModulDownload($URL,$ID);
		foreach((array)$Mod['MODUL']['REQUIRED'] as $k => $v )
		{
			if(!is_file("tmp/download/{$k}.zip"))
			{
				$this->printlog("download required Modul {$k} => {$v['URL']}");
				$this->rModulDownload($v['URL'],$k);
			}
		}
	}
	
	#Lade ein Modul herunter
	#ID, URL = SERVER_URL(GitLab)
	function ModulDownload($URL, $ID, $D=null)
	{
		if(!is_dir('tmp/download'))
			mkdir('tmp/download',0777,1);
		$this->printlog("start download: {$URL}/{$ID}/repository/archive.zip?ref=master => tmp/download/{$ID}.zip");
		copy("{$URL}/{$ID}/repository/archive.zip?ref=master","tmp/download/{$ID}.zip");
	}
	
	function unzipModulDownload($ID, $D=null)
	{
		CFile::remove("system/{$ID}");
		CFile::unzip("tmp/download/{$ID}.zip", "system/{$ID}/");
		$d = CFile::dir("system/{$ID}/");
		CFile::copy("system/{$ID}/{$d[0]}/","system/{$ID}/");
		CFile::remove("system/{$ID}/{$d[0]}");#ToDo: Kontainer Ordner wird nicht gelöscht!
		unlink("tmp/download/{$ID}.zip");
	}
	
	#Fügt Modul-Klassen im tmp zusammen
	function MergeModulClass($D)
	{
		CFile::remove("tmp/system");
		foreach((array)$D['CONFIG']['MODUL']['D'] AS $kM => $vM)
		{
			$MODUL = $vM;
			if($MODUL['ACTIVE'])
			{
				#1.copy library
				if(is_dir("system/{$kM}/library"))
					if($D['CONFIG']['D']['SYMLINK'])
						CFile::symlink("system/{$kM}/library/", "tmp/system/library/");
					else
						CFile::copy("system/{$kM}/library/", "tmp/system/library/");
				
				#1.1. copy tpl
				if(is_dir("system/{$kM}/tpl"))
					if($D['CONFIG']['D']['SYMLINK'])
						CFile::symlink("system/{$kM}/tpl/", "tmp/system/tpl/");
					else
						CFile::copy("system/{$kM}/tpl/", "tmp/system/tpl/");
				
				#2.crate class
				$mod = CFile::_dir(['PATH' => "system/{$kM}/"]);
				foreach((array)$mod['FILE'] AS $kF => $vF)
					if($mod['FILE'][ $kF ]['EXTENSION'] == 'php')
						$D1['FILE'][ $mod['FILE'][ $kF ]['FILENAME'] ]['MODUL'][ $kM ] = $MODUL;
				
				#3.crate page class
				$mod = CFile::_dir(['PATH' => "system/{$kM}/class/"]);
				foreach((array)$mod['FILE'] AS $kF => $vF)
					if($mod['FILE'][ $kF ]['EXTENSION'] == 'php')
						$D2['FILE'][ $mod['FILE'][ $kF ]['FILENAME'] ]['MODUL'][ $kM ] = $MODUL;
			}
		}
		$this->_getclassmerge($D1);
		$D2['TO'] = 'class';
		$this->_getclassmerge($D2);
	}
	
	#Führt je Installiertes Modul aktuelle updates aus die noch austehen
	function UpdateModul($D)
	{
		CFile::write('tmp/system/update.php','<?php
class update
{
	function __construct(){ global $C, $D; $this->C = &$C; $this->D = &$D; }
	function __call($m, $a){ return $a[0]; } 
	
	function load()
	{
		$c = explode(\'__\',$this->D[\'CLASS\']);
		include_once("system/{$c[0]}/update/{$c[1]}.php");
		$cup = new $this->D[\'CLASS\']();
		$cup->up();
		exit(\'1\');
	}
}');
		foreach((array)$D['CONFIG']['MODUL']['D'] AS $k => $v)
		{
			if($v['ACTIVE'])
			{
				$up = CFile::_dir(['PATH' => "system/{$k}/update/"]);
				foreach((array)$up['FILE'] AS $kF => $vF)
				{
					$d = explode('_',$vF['NAME']);
					if($d[0] > $D['CONFIG']['MODUL']['D'][$k]['UPDATE']['LAST_DATE'])
					{
						$UPD[$d[0].$k] = [
						'DATETIME'		=> $d[0],
						'UPDATE_URL'	=> dirname($_SERVER['HTTP_REFERER'])."/index.php?D[PAGE]=update&D[CLASS]={$k}__{$vF['FILENAME']}",
						'MODUL_ID'		=> $k,
						'MODUL_NAME'	=> $vF['NAME'],
						];
						/*
						$this->printlog('update :'.dirname($_SERVER['HTTP_REFERER'])."/index.php?D[PAGE]=update&D[CLASS]={$k}__{$vF['FILENAME']}");
						if(file_get_contents(dirname($_SERVER['HTTP_REFERER'])."/index.php?D[PAGE]=update&D[CLASS]={$k}__{$vF['FILENAME']}") == '1')
							$D['CONFIG']['MODUL']['D'][$k]['UPDATE']['LAST_DATE'] = $d[0];
						else
							exit("ERROR: {$k}__{$vF['NAME']}");
						*/
					}
				}
			}
		}
		#print_R($UPD);
		if(is_array($UPD))
		{
			ksort($UPD);
			#Updates werden nach einander unabhängig vom Modul ausgeführt nach Zeitstempel
			foreach((array)$UPD AS $k => $v)
			{
				if(file_get_contents($v['UPDATE_URL']) == '1')
				{
					$D['CONFIG']['MODUL']['D'][$v['MODUL_ID']]['UPDATE']['LAST_DATE'] = $v['DATETIME'];
					$this->printlog('update :'.$v['UPDATE_URL'].' done');
				}
				else
				{
					$this->printlog('update :'.$v['UPDATE_URL'].' error - stop update');
					exit("ERROR: {$v['MODUL_ID']}__{$v['MODUL_NAME']}");
				}
			}
		}
		return $D;
	}
	
	function setConfig($D)
	{
		$fle = "<?php\r\n";
		foreach((array)$D['CONFIG']['D'] AS $k => $v)
		{
			$cnf = serialize($v);
			$fle .= '$D[\'CONFIG\'][\'D\'][\''.$k.'\'] = unserialize(\''.$cnf.'\');'."\r\n";
		}
		foreach((array)$D['CONFIG']['MODUL']['D'] AS $k => $v)
		{
			$cnf = serialize($v);
			$fle .= '$D[\'CONFIG\'][\'MODUL\'][\'D\'][\''.$k.'\'] = unserialize(\''.$cnf.'\');'."\r\n";
		}
		file_put_contents('!config.php',$fle);
	}
	
	function getConfig()
	{
		if(file_exists('!config.php'))
			include_once('!config.php');
		return $D;
	}
	
	function printlog($text=null)
	{
		echo "<script>log('{$text}');</script>";
	}
	
	private function _getclassmerge($D)
	{	
		ksort($D['FILE']);
		foreach((array)$D['FILE'] AS $kF => $vF)
		{
			$DATE = date('Y.m.d H:i:s');
			$FILE = $kF;
			$R .= "<?php\n#===CLASS {$FILE} START {$DATE} ===\n";
			$m = 0;
			foreach((array)$D['FILE'][ $kF ]['MODUL'] AS $kM => $vM)
			{
				$MODUL = $kM;
				$Ex = ($D['TO'])? "__{$D['TO']}":'';
				if($m == 0)#Anfang
				{

					$kfg = explode('__',$FILE);
					$parentFile = ($FILE[strlen($FILE)-1] == '_')?substr($FILE,0, -(strlen($kfg[count($kfg)-2])+3) ) : substr($FILE,0, -(strlen($kfg[count($kfg)-1])) );
				
					if($FILE != 'index' && is_file("tmp/system/{$D['TO']}/{$parentFile}_.php") )
					{
						$R .= "include('tmp/system/{$parentFile}_.php'); class {$MODUL}{$Ex}__{$FILE}__parent extends {$parentFile}_ { function __construct(){ parent::{__function__}(); } }\n";
					}
					else #Parent nicht verfügbar
					{
						$R .= 'class '."{$MODUL}{$Ex}__{$FILE}".'__parent { function __construct(){ global $C, $D; $this->C = &$C; $this->D = &$D; } function __call($m, $a){ return $a[0]; } }'."\n";
					}
					$R .= "include('system/{$MODUL}/{$D['TO']}/{$FILE}.php');\n";
					$_PRE_MODUL = $MODUL;
					$m++;
				}
				else #Mitte
				{
					$R .= "class {$MODUL}{$Ex}__{$FILE}__parent extends {$_PRE_MODUL}{$Ex}__{$FILE}{ function __construct(){ parent::{__function__}(); } }\n";
					$R .= "include('system/{$MODUL}/{$D['TO']}/{$FILE}.php');\n";
					$_PRE_MODUL = $MODUL;
				}
				
			}
			$R .= "class {$FILE} extends {$MODUL}{$Ex}__{$FILE} { function __construct(){ parent::{__function__}(); } }\n";
		
			$R .= "#===CLASS {$FILE} END =========================";
			
			CFile::mkdir("tmp/system/{$D['TO']}/");
			CFile::write("tmp/system/{$D['TO']}/{$FILE}.php",$R);
			$R = '';
		}
		
		return $R;
	}
}
#================================================
$CPApp = new CPApp();

#Login Check
if(file_exists("!config.php"))
{
	if($_SESSION['LOGIN'] != 'OK')
	{
		include_once("!config.php");
		if($D['LOGIN'] == $D['CONFIG']['D']['ROOT_PASSWORD'])
		{
			$_SESSION['LOGIN'] = 'OK';
		}
		else
		{
			echo "
				<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' crossorigin='anonymous'>
				<table style='height:100%;width:100%'>
					<tr><td style='width:33%'></td><td style='width:33%'></td><td style='width:33%'></td></tr>
					<tr><td></td><td>
						<center>
							<form method='post'>
								<div class='input-group input-group-lg'>
									<span class='input-group-addon'>Password</span>
									<input txpe='password' class='form-control' name='D[LOGIN]'>
									<span class='input-group-btn'>
										<button class='btn btn-default'>login</button>
									</span>
								</div>
							</form>
						</center>
					</td><td></td></tr>
					<tr><td></td><td></td><td></td></tr>
				</table>
			";
			exit;
		}
	}
	elseif($D['LOGOUT']==1)
	{
		$_SESSION['LOGIN'] = null;
		exit("<meta http-equiv='refresh' content='0; URL=install.php'>");
	}
}
		
switch($D['ACTION'])
{
	case '0': #Show Module die hauptsechlich installiert werden -> $MODUL
	
		if(file_exists("!config.php"))
			exit("<script>setTimeout(function(){ loadPage(20);},100);</script>");
		
		echo "<div class='list-group'>";
		foreach((array)$MODUL as $k => $v)
		{
			$Mod = $CPApp->getModulInfo($v['URL'],$k);
			if($Mod)
			{
				echo "<div class='list-group-item'><h4 class='list-group-item-heading'>{$Mod['TITLE']}</h4>";
				echo "<p class='list-group-item-text'><div>".nl2br($Mod['DESCRIPTION'])."</div></p>";
				echo "</div>";
				$CPApp->printlog("load modul information for {$k} done");
			}
			else
				$CPApp->printlog("Error: {$k} Server can not connect. ({$v['URL']}) <label onclick=\"setTimeout(function(){ loadPage({$D['ACTION']});},100);\" style=\"cursor:pointer;\">try again</label>");
		}
		echo "</div>";
		exit();
		break;
	case '10': #Erstelle Ordner Struktur
		$CPApp->printlog("Start: Install");
		$CPApp->createPAppDir();
		$CPApp->printlog("Create dir data, system, tmp done");
		exit("<script>setTimeout(function(){ loadPage(".($D['ACTION']+1).");},100);</script>");#Next Stepp
		break;
	case '11': #Create class, index, .htaccess
		$CPApp->createPAppFile();
		$CPApp->printlog("Create: class.php, index.php, .htaccess");
		exit("<script>setTimeout(function(){ loadPage(".($D['ACTION']+1).");},100);</script>");#Next Stepp
		break;
	case '13': #Download Modules
		foreach((array)$MODUL as $k => $v )
		{
			$CPApp->rModulDownload($v['URL'],$k);
			$CPApp->printlog("download moduls: {$k} done");
		}
		exit("<script>setTimeout(function(){ loadPage(".($D['ACTION']+1).");},100);</script>");#Next Stepp
		break;
	case '14': #Unzip restliche Module
		$d = CFile::dir("tmp/download/");
		for($f=0;$f < count($d);$f++)
		{
			$file = basename($d[$f], ".zip");
			$CPApp->unzipModulDownload($file);
			$CPApp->printlog("unzip moduls: {$file} done");
		}
		if(is_dir('tmp/download'))
			rmdir("tmp/download");
		
		exit("<script>setTimeout(function(){ loadPage(".($D['ACTION']+1).");},100);</script>");#Next Stepp
		break;
	case '20': #Vor Modul Installtion Einstellungsabfrage
		if(file_exists("!config.php"))
			include('!config.php');
		$d = CFile::dir("system/");
		
		echo "
			<div class='row'>
				<div class='col-sm-6'>
					<ul class='list-group'>
						<li class='list-group-item'>
							<div class='input-group'>
								<span class='input-group-addon'>Install Passwort</span>
								<input type='text' name='D[CONFIG][D][ROOT_PASSWORD]' class='form-control' value='".$D['CONFIG']['D']['ROOT_PASSWORD']."'>
							</div>
						</li>
					</ul>
				</div>
				<div class='col-sm-6'>
					<ul class='list-group'>
						<li class='list-group-item'>
							<div class='input-group'>
								<span class='input-group-addon' style='max-width:30px;'><input type='checkbox' id='D[CONFIG][D][SYMLINK]' name='D[CONFIG][D][SYMLINK]' value='1' ".($D['CONFIG']['D']['SYMLINK']?'checked':'')."></span>
								<span class='input-group-addon'><label for='D[CONFIG][D][SYMLINK]'>Symlink nutzen</label></span>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<table class='table'>
					<thead>
						<th>aktive</th>
						<th>Modul</th>
						<th>Type</th>
						<th>Setting</th>
					</thead>
					<tbody>";
		for($f=0;$f < count($d);$f++)
		{
			$mID = $d[$f];
			if(is_dir("system/{$mID}/"))
			{
				$info = $CPApp->getModulInfo('local',$mID);
				$CNF = $info['CONFIG'];
				echo "								
					<tr id='tr{$mID}'>
						<td><input type='hidden' id='D[CONFIG][MODUL][D][{$mID}][ACTIVE][VALUE]' name='D[CONFIG][MODUL][D][{$mID}][ACTIVE][VALUE]' value='".(isset($D['CONFIG']['MODUL']['D'][$mID]['ACTIVE']['VALUE'])?$D['CONFIG']['MODUL']['D'][$mID]['ACTIVE']['VALUE']:1)."'>
							<input onclick=\"document.getElementById('D[CONFIG][MODUL][D][{$mID}][ACTIVE][VALUE]').value = this.checked?1:0\" type='checkbox' ".(isset($D['CONFIG']['MODUL']['D'][$mID]['ACTIVE']['VALUE'])? (($D['CONFIG']['MODUL']['D'][$mID]['ACTIVE']['VALUE'])?'checked':''):'checked').">
						</td>
						<td>{$mID}</td>
						<td>{$info['TYPE']}</td>
						<td>";
						
				if($CNF)
				{	
					#echo "<div class='panel panel-default'><div class='panel-body'>";
					foreach((array)$CNF as $k1 => $v1 )
					{
						if($v1['VALUE'])
						{
							echo "
								<div class='input-group' style='width:100%;'>
									<span class='input-group-addon' style='width:30%;text-align:left;'>".(isset($v1['TITLE'])?$v1['TITLE']:$k1)."</span>
									<input class='form-control' name='D[CONFIG][MODUL][D][{$mID}][{$k1}][VALUE]' value='".($D['CONFIG']['MODUL']['D'][$mID][$k1]['VALUE']?$D['CONFIG']['MODUL']['D'][$mID][$k1]['VALUE']:$v1['VALUE'])."'>
								</div>";
						}
					}
					#echo "</div></div>";
					
					foreach((array)$CNF as $k1 => $v1 )
					{
						if(!$v1['VALUE'])
						{
							foreach((array)$v1['D'] as $k2 => $v2 )
							{
								echo "<div class='panel panel-default'><div style='margin:5px 0 0 20px;text-align:center;'>{$v1['TITLE']} - {$k2}</div><div class='panel-body'>";
									foreach((array)$v2 as $k3 => $v3 )
									{
										echo "<div class='input-group' style='width:100%;'>
												<span class='input-group-addon' style='width:30%;text-align:left;'>".(isset($v3['TITLE'])?$v3['TITLE']:$k3)."</span>
												<input class='form-control' name='D[CONFIG][MODUL][D][{$mID}][{$k1}][D][{$k2}][{$k3}][VALUE]' value='".($D['CONFIG']['MODUL']['D'][$mID][$k1]['D'][$k2][$k3]['VALUE']?$D['CONFIG']['MODUL']['D'][$mID][$k1]['D'][$k2][$k3]['VALUE']:$v3['VALUE'])."'>
											</div>";
									}
								echo "</div></div>";
							}
						}
					}
				}
						
						
						echo "</td>
						
					</tr>";
			}
		}
			echo "
					</tbody>
				</table>
			
		";
		$CPApp->printlog("load Modul Settings");
		exit();
		break;
	case '21':
		$CPApp->setConfig($D);
		$CPApp->printlog("create !config.php done");
		exit("<script>setTimeout(function(){ loadPage(".($D['ACTION']+1).");},100);</script>");
		break;
	case '40': #Installiere Modul
		if(file_exists("!config.php"))
			include('!config.php');
		$CPApp->MergeModulClass($D);
		$CPApp->printlog("Merge Modul Class done");
		exit("<script>setTimeout(function(){ loadPage(".($D['ACTION']+1).");},100);</script>");#Next Stepp
		break;
	case '45': #UPDATE
		if(file_exists("!config.php"))
			include('!config.php');
		$D = $CPApp->UpdateModul($D);
		#$CPApp->setConfig($D);#ToDo: beim release freigeben
		$CPApp->printlog("create modul.php done");
		$CPApp->printlog("update Moduls done");
		exit("<script>setTimeout(function(){ loadPage(".($D['ACTION']+1).");},100);</script>");
		break;
	case '100':
		echo "install done";
		exit();
		break;
	default:
		if(isset($D['ACTION']))
			exit( (($D['ACTION']>0 && $D['ACTION'] < 100)?"<script>setTimeout(function(){ loadPage(".($D['ACTION']+1).");},100);</script>" :'') );
		break;
}
?>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
		<script>
		var pageID = 0;
		function loadPage(pageindex)
		{
			pageID = pageindex;
			//$( "#btnInstall" ).prop( "disabled", true );
			$.ajax({ type: 'POST', dataType : 'html', 
				url : 'install.php?D[ACTION]='+pageindex, data : $('#form1').serialize(),
				success: function(html){
					$('#page').html(html);
					//$( "#btnInstall" ).prop( "disabled", false );
				}
			});
			//$('#page').load('install.php?D[ACTION]='+pageindex);
			$('#loading').html(pageindex);
			$('.progress-bar').width(pageindex+'%');
		}
		function log(text)
		{
			var d = new Date();
			$('#log').append(padLeft(d.getHours(),2)+':'+padLeft(d.getMinutes(),2)+':'+padLeft(d.getSeconds(),2)+' - '+padLeft(pageID,2)+' - '+text+ '<br>');
			$('#log_window').animate({scrollTop: $('#log').height()}, 800);
		}
		function padLeft(nr, n, str){
			return Array(n-String(nr).length+1).join(str||'0')+nr;
		}
		</script>
    </head>
    <body style="margin:5px;background:ghostwhite;" onload="loadPage(0);">
        <form id="form1" method="post">
			<div class="panel panel-default" style="width:80%;margin:50px auto;">
				<div class="panel-heading">
					phpApp Installer 1.0.10
					<div style="float:right;width:20%;">
						<div class="progress">
							<div class="progress-bar" role="progressbar" style="width: 0%;"><label id='loading'></label>%</div>
						</div>
					</div>
				</div>
				<div class="panel-body" id="page"></div>
				<div class="panel-footer">
					<div class="text-right">
					<button class="btn btn-default" type="button" onclick="loadPage(pageID+1);" id="btnInstall">Next</button>
					<button class="btn btn-default" type="button" onclick="loadPage(pageID);">reload</button>
					<button class="btn btn-default" type="button" onclick="window.open('?D[LOGOUT]=1','_self');">LOGOUT</button>
					</div>
				</div>
				<ul class="list-group">
					<li class="list-group-item">
						<div id="log_window" style="overflow-y:scroll;height:100px;">
							<div id="log"></div>
						</div>
					</li>
				</ul>
			</div>
		</form>
	</body>
</html>